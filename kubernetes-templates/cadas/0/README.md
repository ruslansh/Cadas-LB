# Cadas multi-host deployment in Rancher 

## Prerequisites
* A running primary host (min. 8GB of memory)
* Requires host(s) with labels app=CADAS Application, app=CADAS Application 1, app=CADAS Postgres DB Cluster
